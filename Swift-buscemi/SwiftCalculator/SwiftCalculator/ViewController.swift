//
//  ViewController.swift
//  SwiftCalculator
//
//  Created by Damiano Giusti on 14/02/17.
//  Copyright © 2017 Damiano Giusti. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var displayLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func numberButtonPressed(_ sender: UIButton) {
        
    }
    
    @IBAction func operatorButtonPressed(_ sender: UIButton) {

    }

    @IBAction func resultsButtonPressed(_ sender: UIButton) {

    }
}


