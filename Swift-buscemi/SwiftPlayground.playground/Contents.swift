//: Playground - noun: a place where people can play

import UIKit

var str = "Hello, playground"


class Square : NSObject {
    var sideLength : Double
    
    init(sideLength: Double) {
        self.sideLength = sideLength
    }
    
    override var description: String {
        return "Square with side = \(self.sideLength)"
    }
}

class Triangle : NSObject {
    var sideA : Double
    var sideB : Double
    var sideC : Double
    
    init(a: Double, b: Double, c: Double) {
        self.sideA = a
        self.sideB = b
        self.sideC = c
    }
    
    override var description: String {
        return "Triangle with a = \(self.sideA), b = \(self.sideB), c = \(self.sideC)"
    }
}

class TriangoloEquilatero : Triangle {
    
    init(cateti: Double, base: Double) {
        super.init(a: cateti, b: cateti, c: base)
    }
}

let square = Square(sideLength: 2)

let triangle = Triangle(a: 2, b: 3, c: 4)

let triangoloEquilatero = TriangoloEquilatero(cateti: 2, base: 3)


// Enum

enum Rank : Int {
    case Primo, Secondo, Terzo, Quarto
    
    func description() -> String {
        switch self {
        case .Primo:
            return "Il primo numero"
        case .Secondo:
            return "Il secondo numero"
        default:
            return "\(self)"
        }
    }
}

Rank.Secondo.description()

enum Suit : Int {
    case Picche, Cuori, Quadri, Fiori
    
    func description() -> String {
        return "\(self)"
    }
}

Suit.Cuori.description()

struct Card {
    var rank : Rank
    var suit : Suit
    
    func simpleDescription() -> String {
        return "\(rank.description()) di \(suit.description())"
    }
}

let dueDiPicche = Card(rank: Rank.Secondo, suit: Suit.Picche)
dueDiPicche.simpleDescription()

class Carta {
    var rank : Rank
    var suit : Suit
    
    init(rank: Rank, suit: Suit) {
        self.rank = rank
        self.suit = suit
    }
    
    func simpleDescription() -> String {
        return "\(rank.description()) di \(suit.description())"
    }
}

let nuovoDueDiPicche = Carta(rank: Rank.Secondo, suit: Suit.Picche)
nuovoDueDiPicche.simpleDescription()


