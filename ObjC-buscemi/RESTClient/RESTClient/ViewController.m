//
//  ViewController.m
//  RESTClient
//
//  Created by Damiano Giusti on 28/02/17.
//  Copyright © 2017 Damiano Giusti. All rights reserved.
//

#import "ViewController.h"

#define kErrorMessage @"errorMessage"


@interface ViewController ()

@property (strong, nonatomic) NSMutableArray *dataset;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    self.dataset = [[NSMutableArray alloc] init];
}


- (void)viewDidAppear:(BOOL)animated {

    NSString *myUrl = @"https://jsonplaceholder.typicode.com/albums";
    
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    
    [self httpGetWithURL:myUrl andCompletionHandler:^(BOOL success, NSArray *response, NSError *error) {
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
        if (success) {
            [self.dataset removeAllObjects];
            [self.dataset addObjectsFromArray:response];
            [self.tableView reloadData];
            
        } else {
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Error on request" message:error.userInfo[kErrorMessage] preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:nil];
            [alert addAction:okAction];
            [self presentViewController:alert animated:YES completion:nil];
        }
    }];
    
}


#pragma mark - Table View

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.dataset count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TableViewCell" forIndexPath:indexPath];
    
    NSDictionary *dictionary = [self.dataset objectAtIndex:indexPath.row];
    [cell.textLabel setText:dictionary[@"title"]];
    return cell;
}

#pragma mark - Http


- (void)httpGetWithURL:(NSString *)url andCompletionHandler:(void (^)(BOOL success, NSArray *response, NSError *error))completionHandler {
    NSCharacterSet *set = [NSCharacterSet URLQueryAllowedCharacterSet];
    NSString *encodedUrl = [url stringByAddingPercentEncodingWithAllowedCharacters:set];
    
    // consente di fare chiamate asincrone ad un URL
    NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    
    [[session dataTaskWithURL:[NSURL URLWithString:encodedUrl] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        BOOL success = !error;
        if (success) {
            // success
            if ([response isKindOfClass:[NSHTTPURLResponse class]]) {
                NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                NSError *jsonError;
                NSArray *jsonResponse = [NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonError];
                
                if (jsonError) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        completionHandler(NO, nil, [NSError errorWithDomain:NSURLErrorDomain
                                                                       code:httpResponse.statusCode
                                                                   userInfo:@{kErrorMessage: @"Error parsing json"}]);
                    });
                } else {
                    // parsing succeeded
                    NSLog(@"%@", jsonResponse);
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        completionHandler(YES, jsonResponse, nil);
                    });
                }
            } else {
                dispatch_async(dispatch_get_main_queue(), ^{
                    completionHandler(NO, nil, [NSError errorWithDomain:NSURLErrorDomain
                                                                   code:0
                                                               userInfo:@{kErrorMessage: @"Response is not valid"}]);
                });
            }
        } else {
            dispatch_async(dispatch_get_main_queue(), ^{
                completionHandler(NO, nil, [NSError errorWithDomain:NSURLErrorDomain
                                                               code:0
                                                           userInfo:@{kErrorMessage: @"Request failed"}]);
            });
            NSLog(@"%@", error);
        }
    }] resume];
}


@end
