//
//  SecondViewController.m
//  NavigationControllerDemo
//
//  Created by Damiano Giusti on 07/02/17.
//  Copyright © 2017 Damiano Giusti. All rights reserved.
//

#import "SecondViewController.h"

@interface SecondViewController ()

@property (weak, nonatomic) IBOutlet UIView *colorView;

@end

@implementation SecondViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.colorView setBackgroundColor:self.viewColor];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidLayoutSubviews {
    CGFloat width = self.colorView.frame.size.width;
    [self.colorView.layer setCornerRadius:width / 2];
}

- (IBAction)backButtonPressed:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:YES];
    
    /*
     Utilizzabile quando non si ha un NavigationController.
     Quando un ViewController viene mostrato senza un NavigationController, viene mostrato in maniera modale.
    [self dismissViewControllerAnimated:true completion:^{
        
    }];
     */
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
