//
//  ViewController.m
//  NavigationControllerDemo
//
//  Created by Damiano Giusti on 07/02/17.
//  Copyright © 2017 Damiano Giusti. All rights reserved.
//

#import "ViewController.h"
#import "SecondViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    __weak ViewController *weakSelf = self;
    [NSTimer scheduledTimerWithTimeInterval:2 repeats:NO block:^(NSTimer * _Nonnull timer) {
        [weakSelf performSegueWithIdentifier:@"viewControllerSegue" sender:weakSelf];
    }];
    /*NSTimer *timer = [[NSTimer alloc] initWithFireDate:[NSDate date] interval:10 repeats:NO block:^(NSTimer * _Nonnull timer) {
       
    }];
    [timer fire];*/
}

- (IBAction)backToFirstViewController1:(UIStoryboardSegue *)sender {
    
}

- (IBAction)backToFirstViewController2:(UIStoryboardSegue *)sender {
    
}

- (void)viewDidAppear:(BOOL)animated {
    /*
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Completed!" message:@"Dismiss completed" preferredStyle:UIAlertControllerStyleActionSheet];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [alert dismissViewControllerAnimated:true completion:nil];
    }];
    [alert addAction:okAction];
    [self presentViewController:alert animated:true completion:nil];
     */
    [self setTitle:@"ciao"];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    UIColor *color = [UIColor blackColor];
    
    if ([sender isKindOfClass:[UIButton class]] && [segue.identifier isEqualToString:@"buttonSegue"]) {
        UIButton *button = (UIButton *) sender;
        color = [UIColor blueColor];
        NSLog(@"Ho premuto il pulsante \"%@\"", button.currentTitle);
    } else if ([segue.identifier isEqualToString:@"viewControllerSegue"]) {
        color = [UIColor redColor];
        NSLog(@"Ho premuto il pulsante \"%@\"", self);
    }
    
    SecondViewController *svc = segue.destinationViewController;
    svc.viewColor = color;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
