//
//  SecondViewController.h
//  NavigationControllerDemo
//
//  Created by Damiano Giusti on 07/02/17.
//  Copyright © 2017 Damiano Giusti. All rights reserved.
//

#import "ViewController.h"

@interface SecondViewController : UIViewController

@property (nonatomic) UIColor *viewColor;

@end
