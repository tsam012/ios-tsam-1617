//: Playground - noun: a place where people can play

import UIKit

let str = "Hello, playground";

public class Person : NSObject {
    private(set) var name: String
    private(set) var surname: String
    public var date: Date?
    
    var displayName : String {
        get {
            return self.name + " " + self.surname
        }
    }
    
    public init(name: String, surname: String) {
        self.name = name
        self.surname = surname
    }
    
    public override var description: String {
        get {
            return displayName
        }
    }
}

let damiano = Person(name: "Damiano", surname: "Giusti")

damiano.displayName

damiano.date = Date()

print(damiano)

let label = "Questa è una stringa"
let larghezza : Int = 90

let larghezzaLabel = label + String(larghezza)
let altraLabel = label + " larga \(larghezza) cm"

var array = [Int]();

for i in 0 ..< 100 {
    array.append(i)
}

for i in 0 ..< array.count {
    print(i)
}

var dictionary = [String:Person]()
dictionary[damiano.displayName] = damiano
dictionary.description

array.map({ (x) -> Int in
    return x + 1
}).reduce(0) { (accum, x) -> Int in
    return accum + x
}


var optionalName : String?
optionalName = "ciao"

print(optionalName ?? "nil")

if let name = optionalName {
    print(name)
} else {
    print("nil")
}


// custom operator

infix operator ❤️ { associativity left precedence 160 }

func ❤️ (person1: String, person2: String) -> String {
    return person1 + " + " + person2 + " = ∞"
}

"Elenia" ❤️ "Damiano"

// switch case

var verdura = "peperoncino"

func evaluateVerdüra(string: String) {
    switch string {
    case "ciao":
        break
    case "ciaone", "proprio":
        break
    case let x where x.hasSuffix("rosso"):
        break
    default:
        break
    }
}

evaluateVerdüra(string: verdura)

extension String {
    public func stringWithUpperCaseCapitalLetter() -> String {
        if self.isEmpty {
            return ""
        }
        return "\(self[self.startIndex])".uppercased() + self.substring(from: self.index(after: self.startIndex)).lowercased()
    }
}

"c".stringWithUpperCaseCapitalLetter()

// methods with a lot of return types

func calcolaStatistiche(punteggi: [Int]) -> (minimo: Int, massimo: Int, somma: Int) {
    
    func add(a: Int, b: Int) -> Int {
        return a + b
    }
    
    return (punteggi.min() ?? Int.min, punteggi.max() ?? Int.min, punteggi.reduce(0, add))
}

func hasAnyMatches(list: [Int], condition: ((Int) -> Bool)) -> Bool {
    return list.filter(condition).count > 0
}

func isOdd(number: Int) -> Bool {
    return number % 2 != 0;
}

Array(0...100).sorted(by: { (a, b) in (a < b) })

hasAnyMatches(list: [2, 4, 3], condition: isOdd)

Array(-10...10).map({ (x) in Double(x)/10.0 }).map({ (x) in (sin(Double(x))) }).forEach({ x in print(x)})


