//
//  ViewController.m
//  calculator
//
//  Created by Damiano Giusti on 25/01/17.
//  Copyright © 2017 Damiano Giusti. All rights reserved.
//

#import "ViewController.h"
#import "CalculatorController.h"

@interface ViewController ()

@property (strong, nonatomic) CalculatorController *calculatorController;

@property (weak, nonatomic) IBOutlet UILabel *displayLabel;

@end

@implementation ViewController

- (void)viewDidLoad {
    self.calculatorController = [[CalculatorController alloc] init];
}

#pragma mark - Actions

- (IBAction)numberButtonPressed:(UIButton *)sender {
    
    [self.calculatorController appendToCurrentNumber:sender.titleLabel.text];
    [self refreshDisplay:[self.calculatorController currentNumber]];
}


- (IBAction)operandButtonPressed:(UIButton *)sender {
    
    BOOL hasOperands = [self.calculatorController hasOperands];
    BOOL hasCurrentNumber = [self.calculatorController hasCurrentNumber];
    NSString *currentNumber = [self.calculatorController currentNumber];
    
    if (hasCurrentNumber) {
        if (hasOperands) {
            currentNumber = [self.calculatorController evaluateExpression];
            [self refreshDisplay:currentNumber];
        }
        [self.calculatorController clearCurrentNumberAndUpdateSecondaryWithNumber:currentNumber];
        
    } else if (hasOperands)
        [self.calculatorController popOperatorFromStack];
    
    [self.calculatorController pushOperatorInStack:sender.titleLabel.text];
}


- (IBAction)resultButtonPressed:(UIButton *)sender {
    
    BOOL hasOperands = [self.calculatorController hasOperands];
    BOOL hasCurrentNumber = [self.calculatorController hasCurrentNumber];
    BOOL hasSecondaryNumber = [self.calculatorController hasSecondaryNumber];
    
    if (hasSecondaryNumber && !hasCurrentNumber) {
        [self.displayLabel setText:[self.calculatorController secondaryNumber]];
        
    } else if (hasOperands && hasSecondaryNumber && hasCurrentNumber) {
        NSString *result = [self.calculatorController evaluateExpression];
        [self.calculatorController clearCurrentNumberAndUpdateSecondaryWithNumber:result];
        [self refreshDisplay:result];
        
    } else {
        [self.displayLabel setText:[self.calculatorController currentNumber]];
    }
}


#pragma mark - UI

- (void)refreshDisplay:(NSString *)text {
    
    [self.displayLabel setText: text];
}


@end
