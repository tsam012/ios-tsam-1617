//
//  CalculatorController.m
//  calculator
//
//  Created by Damiano Giusti on 31/01/17.
//  Copyright © 2017 Damiano Giusti. All rights reserved.
//

#import "CalculatorController.h"

@interface CalculatorController()

@property (strong, nonatomic) NSMutableArray *numbers;

@property (strong, nonatomic) NSMutableArray *operandsStack;

@end

@implementation CalculatorController

// private
- (NSMutableArray *)numbers {
    if (!_numbers)
        _numbers = @[@"", @""].mutableCopy;
    return _numbers;
}


// private
- (NSMutableArray *)operandsStack {
    if (!_operandsStack)
        _operandsStack = [[NSMutableArray alloc] init];
    return _operandsStack;
}


- (void)pushOperatorInStack:(NSString *)number {
    [self.operandsStack addObject:number];
}


- (NSString *)popOperatorFromStack {
    NSString *item = [self.operandsStack lastObject];
    [self.operandsStack removeLastObject];
    return item;
}


- (NSString *)currentNumber {
    return self.numbers[1];
}


- (void)appendToCurrentNumber:(NSString *)number {
    self.numbers[1] = [self.numbers[1] stringByAppendingString:number];
}


- (void)clearCurrentNumber {
    self.numbers[1] = @"";
}


- (NSString *)secondaryNumber {
    return self.numbers[0];
}


- (void)setSecondaryNumber:(NSString *)number {
    self.numbers[0] = number;
}


- (BOOL)hasOperands {
    return [self.operandsStack count] > 0;
}


- (BOOL)hasCurrentNumber {
    return [[self currentNumber] length];
}


- (BOOL)hasSecondaryNumber {
    return [[self secondaryNumber] length];
}


#pragma mark - Calculations


- (NSString *)toString:(double)value {
    
    return [NSString stringWithFormat:@"%g", value];
}

- (NSString *)evaluateExpression {
    return [self toString:[self evaluateExpressionInternal]];
}

/**
 Evaluates the expression using the current operator, the current number and the secondary number
 */
- (double)evaluateExpressionInternal {
    
    if ([self.operandsStack count]) {
        NSString *operand = [self.operandsStack lastObject];
        [self.operandsStack removeLastObject];
        return [self operandForSymbol:operand]([[self secondaryNumber] doubleValue], [[self currentNumber] doubleValue]);
    }
    return [self.numbers[1] doubleValue];
}


/**
 Sets secondary number's value to the given value and clears the current
 */
- (void)clearCurrentNumberAndUpdateSecondaryWithNumber:(NSString *)number {
    
    [self setSecondaryNumber:number];
    [self clearCurrentNumber];
}


- (double(^)(double,double))operandForSymbol:(NSString *)symbol {
    if ([symbol isEqualToString:@"+"])
        return ^double(double item1, double item2) {
            return item1 + item2;
        };
    if ([symbol isEqualToString:@"-"])
        return ^double(double item1, double item2) {
            return item1 - item2;
        };
    if ([symbol isEqualToString:@"×"])
        return ^double(double item1, double item2) {
            return item1 * item2;
        };
    if ([symbol isEqualToString:@"÷"])
        return ^double(double item1, double item2) {
            return item1 / item2;
        };
    
    return nil;
}

@end
