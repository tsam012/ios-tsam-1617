//
//  CalculatorController.h
//  calculator
//
//  Created by Damiano Giusti on 31/01/17.
//  Copyright © 2017 Damiano Giusti. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CalculatorController : NSObject

- (void)pushOperatorInStack:(NSString *)number;

- (NSString *)popOperatorFromStack;

- (NSString *)currentNumber;

- (void)appendToCurrentNumber:(NSString *)number;

- (void)clearCurrentNumber;

- (NSString *)secondaryNumber;

- (void)setSecondaryNumber:(NSString *)number;

- (BOOL)hasOperands;

- (BOOL)hasCurrentNumber;

- (BOOL)hasSecondaryNumber;

- (NSString *)evaluateExpression;

- (void)clearCurrentNumberAndUpdateSecondaryWithNumber:(NSString *)number;

@end
