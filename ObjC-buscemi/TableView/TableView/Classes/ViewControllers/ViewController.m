//
//  ViewController.m
//  TableView
//
//  Created by Damiano Giusti on 21/02/17.
//  Copyright © 2017 Damiano Giusti. All rights reserved.
//

#import "ViewController.h"

#import "MainTableViewDelegate.h"

@interface ViewController ()

@property (weak, nonatomic) IBOutlet UITableView *firstTableView;
@property (weak, nonatomic) IBOutlet UITableView *secondTableView;

@property (strong, nonatomic) MainTableViewDelegate *firstTableViewDelegate;
@property (strong, nonatomic) MainTableViewDelegate *secondTableViewDelegate;

@property (strong, nonatomic) NSMutableArray *firstDataset;
@property (strong, nonatomic) NSMutableArray *secondDataset;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.firstDataset = @[@"Ciao", @"Sono", @"Il", @"Primo", @"Dataset"].mutableCopy;
    self.secondDataset = @[@"Ciao", @"Sono", @"Il", @"Secondo", @"Dataset"].mutableCopy;
    
    self.firstTableViewDelegate = [[MainTableViewDelegate alloc] initWithDataset:self.firstDataset andReuseIdentifier:@"FirstTableViewCell"];
    self.secondTableViewDelegate = [[MainTableViewDelegate alloc] initWithDataset:self.secondDataset andReuseIdentifier:@"SecondTableViewCell"];
    
    [self.firstTableView setDelegate:self.firstTableViewDelegate];
    [self.firstTableView setDataSource:self.firstTableViewDelegate];
    
    [self.secondTableView setDelegate:self.secondTableViewDelegate];
    [self.secondTableView setDataSource:self.secondTableViewDelegate];
}

@end
