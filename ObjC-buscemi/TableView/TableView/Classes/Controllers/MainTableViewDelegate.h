//
//  FirstTableViewDelegate.h
//  TableView
//
//  Created by Damiano Giusti on 21/02/17.
//  Copyright © 2017 Damiano Giusti. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "UIKit/UIKit.h"

@interface MainTableViewDelegate : NSObject <UITableViewDelegate, UITableViewDataSource>

@property (strong, nonatomic) NSArray *dataset;

- (instancetype)initWithDataset:(NSArray *)dataset
             andReuseIdentifier:(NSString *)identifier;

@end
