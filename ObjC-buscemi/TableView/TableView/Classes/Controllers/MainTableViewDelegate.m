//
//  FirstTableViewDelegate.m
//  TableView
//
//  Created by Damiano Giusti on 21/02/17.
//  Copyright © 2017 Damiano Giusti. All rights reserved.
//

#import "MainTableViewDelegate.h"

@interface MainTableViewDelegate()

@property (strong, nonatomic) NSString *reuseIdentifier;

@end

@implementation MainTableViewDelegate

- (instancetype)initWithDataset:(NSArray *)dataset
             andReuseIdentifier:(NSString *)identifier {
    self = [super init];
    
    if (self) {
        _dataset = dataset;
        _reuseIdentifier = identifier;
    }
    
    return self;
}


#pragma mark - Table View


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.dataset count];
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:self.reuseIdentifier forIndexPath:indexPath];
    
    
    [cell.textLabel setText:[self.dataset objectAtIndex:indexPath.row]];
    
    return cell;
}


@end
