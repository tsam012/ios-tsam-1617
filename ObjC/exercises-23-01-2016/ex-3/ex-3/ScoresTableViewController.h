//
//  ScoresTableViewController.h
//  ex-3
//
//  Created by Damiano Giusti on 24/01/17.
//  Copyright © 2017 Damiano Giusti. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "DataStore.h"

@protocol ScoreTableViewDelegate <NSObject>

- (NSArray*) scoreTableViewDidRequestDataset;

@end


@interface ScoresTableViewController : UITableViewController

@property (unsafe_unretained) id <ScoreTableViewDelegate> delegate;

@end
