//
//  DataStore.h
//  ex-3
//
//  Created by Damiano Giusti on 24/01/17.
//  Copyright © 2017 Damiano Giusti. All rights reserved.
//

#import <Foundation/Foundation.h>

FOUNDATION_EXPORT const NSString *Date = @"date";
FOUNDATION_EXPORT const NSString *Success = @"success";
FOUNDATION_EXPORT const NSString *IsEven = @"isEven";
FOUNDATION_EXPORT const NSString *Number = @"number";

@interface DataStore : NSObject

+ (void)saveScore:(bool)correct withNumber:(NSInteger)number;

+ (NSArray*)loadScores;

+ (NSArray*)loadScoresSortedByDateDescending;

@end
