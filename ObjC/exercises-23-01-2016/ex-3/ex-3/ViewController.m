//
//  ViewController.m
//  ex-3
//
//  Created by Damiano Giusti on 23/01/17.
//  Copyright © 2017 Damiano Giusti. All rights reserved.
//

#import "ViewController.h"
#import "DataStore.h"

#define ErrorMessage @"Incorrect answer! Did you attend primary school?"
#define SuccessMessage @"Your answer is correct!"

@interface ViewController () {
    NSInteger number;
    bool isNumerEven;
}

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)viewDidAppear:(BOOL)animated {
    [self initNumber];
    
    [self setTitle:@"EvenOdd Game"];
    
    UIBarButtonItem *scoresButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemBookmarks target:self action:@selector(scoresButtonPressed)];
    self.navigationItem.rightBarButtonItem = scoresButton;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Game

- (void)initNumber {
    number = [self getRandomNumberBetween:0 to:100];
    isNumerEven = number % 2 == 0;
    [self.messageLabel setText:[NSString stringWithFormat:@"Is the number %li even?", (long)number]];
}

- (bool)evaluateResponse:(bool)userDidPressYes {
    NSInteger userNumber = [self.numberTextField.text integerValue];
    bool isEven = userNumber % 2 == 0;
    
    return userDidPressYes && isNumerEven;
}

- (void)presentDialog:(bool)success {
    UIAlertController *dialog = [UIAlertController alertControllerWithTitle:@"Result" message:nil preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *action = nil;
    void (^block)(UIAlertAction * action) = ^(UIAlertAction * _Nonnull action) {
        [self initNumber];
    };
    if (success) {
        action = [UIAlertAction actionWithTitle:@"Thanks!" style:UIAlertActionStyleDefault handler:block];
        [dialog setMessage:SuccessMessage];
    } else {
        action = [UIAlertAction actionWithTitle:@"No" style:UIAlertActionStyleDefault handler:block];
        [dialog setMessage:ErrorMessage];
    }
    
    [dialog addAction:action];
    [self presentViewController:dialog animated:true completion:nil];
}

#pragma mark - Actions

- (IBAction)buttonYesPressed:(id)sender {
    bool success = [self evaluateResponse: true];
    [DataStore saveScore:success withNumber:number];
    [self presentDialog:success];
}

- (IBAction)buttonNoPressed:(id)sender {
    [self presentDialog:[self evaluateResponse: false]];
}

- (void)scoresButtonPressed {
    ScoresTableViewController *scoresController = [[ScoresTableViewController alloc] init];
    scoresController.delegate = self;
    [self.navigationController pushViewController:scoresController animated:true];
}

#pragma mark - Utils

- (NSInteger)getRandomNumberBetween:(NSInteger)from to:(NSInteger)to {
    return from + arc4random() % (to - from + 1);
}

#pragma mark - ScoreTableViewDelegate

- (NSArray*)scoreTableViewDidRequestDataset {
    return [DataStore loadScores];
}

@end
