//
//  DataStore.m
//  ex-3
//
//  Created by Damiano Giusti on 24/01/17.
//  Copyright © 2017 Damiano Giusti. All rights reserved.
//

#import "DataStore.h"

#define Defaults [NSUserDefaults standardUserDefaults]
#define ScoresKey @"Scores"

@implementation DataStore

+ (void)saveScore:(bool)correct withNumber:(NSInteger)number {
    NSMutableArray *mutableArray = [[Defaults objectForKey:ScoresKey] mutableCopy];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc ] init];
    dict[Date] = @([NSDate date].timeIntervalSince1970);
    dict[Success] = @(correct);
    dict[IsEven] = @(number % 2 == 0);
    dict[Number] = @(number);
    
    if (mutableArray == nil)
        mutableArray = @[].mutableCopy;
    NSLog(@"%@", mutableArray);
    [mutableArray addObject:dict];
    
    [Defaults setObject:mutableArray forKey:ScoresKey];
}

+ (NSArray*)loadScores {
    return [Defaults objectForKey:ScoresKey];
}

+ (NSArray*)loadScoresSortedByDateDescending {
    return [[Defaults objectForKey:ScoresKey] sortedArrayUsingComparator:^NSComparisonResult(NSMutableDictionary *obj1, NSMutableDictionary *obj2) {
        NSString *interval1 = obj1[Date];
        NSString *interval2 = obj2[Date];
        return [interval1 compare:interval2 options:NSBackwardsSearch];
    }];
}

@end
