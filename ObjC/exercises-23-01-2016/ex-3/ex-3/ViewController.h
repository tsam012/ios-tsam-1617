//
//  ViewController.h
//  ex-3
//
//  Created by Damiano Giusti on 23/01/17.
//  Copyright © 2017 Damiano Giusti. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ScoresTableViewController.h"

@interface ViewController : UIViewController <ScoreTableViewDelegate>

@property (nonatomic, weak) IBOutlet UILabel *messageLabel;

@property (nonatomic, weak) IBOutlet UITextField *numberTextField;

- (IBAction)buttonYesPressed:(id)sender;

- (IBAction)buttonNoPressed:(id)sender;


@end

