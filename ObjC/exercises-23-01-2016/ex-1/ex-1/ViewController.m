//
//  ViewController.m
//  ex-1
//
//  Created by Damiano Giusti on 23/01/17.
//  Copyright © 2017 Damiano Giusti. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)viewDidAppear:(BOOL)animated {
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Actions

- (IBAction)nameTextFieldDidChangeText:(UITextField *)sender {
    NSLog(@"%@", sender.text);
    [self.nameLabel setText:sender.text];
}

- (IBAction)nameTextFieldDidEndOnExit:(UITextField *)sender {
    NSLog(@"end on exit");
}

@end
