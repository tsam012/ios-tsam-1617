//
//  ViewController.m
//  ex-4
//
//  Created by Damiano Giusti on 23/01/17.
//  Copyright © 2017 Damiano Giusti. All rights reserved.
//

#import "ViewController.h"

#define Count 100
#define FOR for (int i = 0; i < Count; ++i)

@interface ViewController () {

    NSMutableArray *array;
}

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    array = @[].mutableCopy;
    [self populateArray];
    
}

- (void)viewDidAppear:(BOOL)animated {
    NSLog(@"%ld\n", (long)[self sumArray]);
    [self printArray:[self sortArray:true]];
    [self printArray:[self sortArray:false]];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)populateArray {
    FOR {
        [array addObject:[self getRandomNumberBetween:0 to:100]];
    }
}

- (NSInteger)sumArray {
    NSInteger sum = 0;
    FOR {
        sum += [array[i] integerValue];
    }
    return sum;
}

- (NSArray*)sortArray:(bool)ascending {
    return [array sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
        if (obj1 == obj2)
            return NSOrderedSame;
        
        if (obj1 > obj2) {
            if (ascending)
                return NSOrderedDescending;
            return NSOrderedAscending;
        }

        // if (obj1 < obj2)
        if (ascending)
            return NSOrderedAscending;
        return NSOrderedDescending;
    }];
}

- (void)printArray:(NSArray*)arrayToPrint {
    FOR {
        NSLog(@"%@", arrayToPrint[i]);
    }
    NSLog(@"\n");
}

- (NSNumber*)getRandomNumberBetween:(int)from to:(int)to {
    NSInteger integer = (from + arc4random() % (to - from + 1));
    return [NSNumber numberWithInteger:integer];
}

@end
