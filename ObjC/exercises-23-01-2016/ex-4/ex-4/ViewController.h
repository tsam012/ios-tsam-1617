//
//  ViewController.h
//  ex-4
//
//  Created by Damiano Giusti on 23/01/17.
//  Copyright © 2017 Damiano Giusti. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (nonatomic, weak) IBOutlet UILabel *resultLabel;

@end

