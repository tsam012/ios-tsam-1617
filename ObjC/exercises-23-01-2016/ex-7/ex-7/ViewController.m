//
//  ViewController.m
//  ex-7
//
//  Created by Damiano Giusti on 24/01/17.
//  Copyright © 2017 Damiano Giusti. All rights reserved.
//

#import "ViewController.h"

@interface ViewController () {
    NSTimer* timer;
    NSInteger counter;
}

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)viewWillAppear:(BOOL)animated {
    [self startTimer];
}

- (void)viewWillDisappear:(BOOL)animated {
    [self pauseTimer];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Timer

- (void)startTimer {
    timer  = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(timerDidTick) userInfo:nil repeats:YES];
}

- (void)pauseTimer {
    [timer invalidate];
    timer = nil;
}

- (void)timerDidTick {
    counter ++;
    NSInteger hours = counter / 3600;
    NSInteger minutes = (counter / 60) % 60;
    NSInteger seconds = counter % 60;
    [self.label setText:[NSString stringWithFormat:@"%02ld:%02ld:%02ld", hours, minutes, seconds]];
}


@end
