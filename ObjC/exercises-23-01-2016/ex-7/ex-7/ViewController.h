//
//  ViewController.h
//  ex-7
//
//  Created by Damiano Giusti on 24/01/17.
//  Copyright © 2017 Damiano Giusti. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (nonatomic, weak) IBOutlet UILabel *label;

@end

