//
//  ViewController.m
//  ex-2
//
//  Created by Damiano Giusti on 23/01/17.
//  Copyright © 2017 Damiano Giusti. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Actions

- (IBAction)numberTextFieldDidEndOnExit:(id)sender {
    [self evaluateButtonPressed:self];
}

- (IBAction)evaluateButtonPressed:(id)sender {
    NSInteger number = [self.numberTextField.text integerValue];
    bool isEven = number % 2 == 0;
    NSString *message = @"The number %i is %@";
    
    if (isEven) {
        message = [NSString stringWithFormat:message, number, @"even"];
    } else {
        message = [NSString stringWithFormat:message, number, @"odd"];
    }
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Evaluate" message:message preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:nil];
    [alertController addAction:okAction];
    
    [self presentViewController:alertController animated:true completion:nil];
    
}

@end
