//
//  ViewController.h
//  ex-2
//
//  Created by Damiano Giusti on 23/01/17.
//  Copyright © 2017 Damiano Giusti. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (nonatomic, weak) IBOutlet UITextField *numberTextField;

- (IBAction)evaluateButtonPressed:(id)sender;

- (IBAction)numberTextFieldDidEndOnExit:(id)sender;

@end

