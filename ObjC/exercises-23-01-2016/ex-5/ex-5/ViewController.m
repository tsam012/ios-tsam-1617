//
//  ViewController.m
//  ex-5
//
//  Created by Damiano Giusti on 23/01/17.
//  Copyright © 2017 Damiano Giusti. All rights reserved.
//

#import "ViewController.h"

#define FOR

typedef NS_ENUM(NSInteger, Attribute) {
    Name,
    Surname
};

@interface ViewController () {
    NSArray *array;
}

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    array = @[
        @"Freddie Campbell",
        @"Krista Kim",
        @"Merle	Hubbard",
        @"Lorraine Weber",
        @"Lorenzo Greene",
        @"Sandra Brock",
        @"Nichole Hines",
        @"Floyd	Clarke",
        @"Andrea Ramsey",
        @"Marcus Reid",
        @"Jacob	Garza",
        @"Peter	Herrera",
        @"Glenn	Horton",
        @"Francis Mack",
        @"Yvette Farmer",
        @"Evan Hughes",
        @"Shelley Lane",
        @"Gladys Carroll",
        @"Stella Thornton",
        @"Mindy	Wilkerson",
        @"Beth Diaz",
        @"Everett Mckinney",
        @"Audrey Cooper",
        @"Christie Sutton",
        @"Timothy Walters"
    ];
}

- (void)viewWillAppear:(BOOL)animated {
    [self printArray:[self sortAlphabetically: array]];
    [self printArray:[self extractAttributes:array forAttribute:Name]];
    [self printArray:[self extractAttributes:array forAttribute:Surname]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)printArray:(NSArray*)arrayToPrint {
    for (int i = 0; i < arrayToPrint.count; ++i) {
        NSLog(@"%@", arrayToPrint[i]);
    }
    NSLog(@"\n");
}

- (NSArray*)sortAlphabetically:(NSArray*)arrayToSort {
    return [arrayToSort sortedArrayUsingComparator:^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2) {
        return [obj1 compare:obj2];
    }];
}

- (NSArray*)extractAttributes:(NSArray*)arrayToSplit forAttribute:(Attribute)attribute {
    NSMutableArray *resultArray = [[NSMutableArray alloc] init];
    
    for (int i = 0; i < arrayToSplit.count; ++i) {
        NSString *string = arrayToSplit[i];
        if ([string rangeOfString:@" "].location != NSNotFound) {
            [resultArray addObject:[string componentsSeparatedByString:@" "][attribute]];
        }
        string = nil;
    }
    return resultArray;
}


@end
