//
//  ViewController.m
//  ex-6
//
//  Created by Damiano Giusti on 24/01/17.
//  Copyright © 2017 Damiano Giusti. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Actions

- (IBAction)submitButtonPressed:(UIButton*)sender {
    NSLog(@"Name:\t%@", self.nameTextField.text);
    NSLog(@"Surname:\t%@", self.surnameTextField.text);
    NSLog(@"Age:\t%@", self.ageTextField.text);
}


@end
