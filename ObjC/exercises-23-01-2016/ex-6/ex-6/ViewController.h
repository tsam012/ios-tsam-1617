//
//  ViewController.h
//  ex-6
//
//  Created by Damiano Giusti on 24/01/17.
//  Copyright © 2017 Damiano Giusti. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (nonatomic, weak) IBOutlet UIButton *submitButton;

@property (nonatomic, weak) IBOutlet UITextField *nameTextField;

@property (nonatomic, weak) IBOutlet UITextField *surnameTextField;

@property (nonatomic, weak) IBOutlet UITextField *ageTextField;

- (IBAction)submitButtonPressed:(UIButton*)sender;

@end

