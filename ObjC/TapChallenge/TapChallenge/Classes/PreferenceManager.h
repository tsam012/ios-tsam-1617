//
//  PreferenceManager.h
//  TapChallenge
//
//  Created by Damiano Giusti on 17/01/17.
//  Copyright © 2017 Damiano Giusti. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "Score.h"

@interface PreferenceManager : NSObject

+ (void)storeResult:(Score *)result;

+ (NSArray *)getResults;

+ (BOOL)hasResult;

+ (void)markAsLaunched;

+ (BOOL)firstAppLaunch;

@end
