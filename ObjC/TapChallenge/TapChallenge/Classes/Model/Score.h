//
//  Score.h
//  TapChallenge
//
//  Created by Damiano Giusti on 10/02/17.
//  Copyright © 2017 Damiano Giusti. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Score : NSObject <NSCoding>

@property (strong, nonatomic) NSString *playerName;

@property (strong, nonatomic, readonly) NSDate *date;

@property (nonatomic, readonly) NSInteger score;

- (instancetype)initWithScore:(NSInteger)score andDate:(NSDate *)date;

@end
