//
//  Score.m
//  TapChallenge
//
//  Created by Damiano Giusti on 10/02/17.
//  Copyright © 2017 Damiano Giusti. All rights reserved.
//

#import "Score.h"

@implementation Score

- (instancetype)initWithScore:(NSInteger)score andDate:(NSDate *)date {
    self  = [super init];
    
    if (self) {
        _score = score;
        _date = date;
    }
    
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)decoder {
    self = [super init];
    
    if (self) {
        _score = [decoder decodeIntegerForKey:@"score"];
        _date = [NSDate dateWithTimeIntervalSince1970:[decoder decodeIntegerForKey:@"date"]];
    }
    
    return self;
}

- (void)encodeWithCoder:(NSCoder *)encoder {
    [encoder encodeInteger:_score forKey:@"score"];
    [encoder encodeInteger:[_date timeIntervalSince1970] forKey:@"date"];
}

- (NSString *)description {
    return [NSString stringWithFormat:@"{ score : %li, date : %@ }", _score, _date];
}

@end
