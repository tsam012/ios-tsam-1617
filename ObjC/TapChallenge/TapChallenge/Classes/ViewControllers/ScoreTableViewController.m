//
//  ScoreTableViewController.m
//  TapChallenge
//
//  Created by Damiano Giusti on 18/01/17.
//  Copyright © 2017 Damiano Giusti. All rights reserved.
//

#import "ScoreTableViewController.h"

#import "Score.h"

@interface ScoreTableViewController ()

@property (strong, nonatomic) UIAlertController *dialog;

@end

@implementation ScoreTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    // ottengo il dataset dal viewcontroller chiamante
    _scoresArray = [[self.delegate scoreTableViewFetchResult] sortedArrayUsingComparator:^NSComparisonResult(Score * _Nonnull obj1, Score * _Nonnull obj2) {
        if (obj1.score == obj2.score)
            return NSOrderedSame;
        if (obj1.score > obj2.score)
            return NSOrderedAscending;
        return NSOrderedDescending;
    }];
    
    [self.delegate scoreTableViewDidFetchResult];
    
    [self setTitle:@"Scores"];
}

- (void)viewWillAppear:(BOOL)animated {
    // forzo il refresh dei dati
//    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // return the number of sections
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
// return the number of rows
    return _scoresArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ScoreTableViewCell" forIndexPath:indexPath];
    
    // NSIndexPath
    // coppia di due valori di tipo intero --> (row, section)

    /*
     Di default la cella ha una UIImageView a sinistra, una UILabel per mostrare testo e una UIAccessoryView,
     che non può essere tolta.
     */
    
    // Configure the cell...
    //NSString *text = [NSString stringWithFormat:@"Section %i row %i", (int)indexPath.section, (int)indexPath.row];
    //[cell.textLabel setText:text];
    
    // gestisco un accessory type
    // cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    NSInteger rowNumber = indexPath.row;
    Score *score = [self scoreForIndexPath:indexPath];
    
    
    cell.textLabel.text = [NSString stringWithFormat:@"Score %li - %@", score.score, [self formatDate:score.date]];
    if (rowNumber == 0) {
        cell.backgroundColor = [UIColor orangeColor];
    } else {
        cell.backgroundColor = [UIColor clearColor];
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    Score *score = [self scoreForIndexPath:indexPath];
    NSString *message = [NSString stringWithFormat:@"At %@ you tapped %li times", [self formatDate:score.date], score.score];
    [self.dialog setMessage:message];
    [self presentViewController:self.dialog animated:true completion:nil];
}

#pragma mark - Dialog

- (UIAlertController *)dialog {
    if (!_dialog) {
        _dialog = [UIAlertController alertControllerWithTitle:@"Score" message:@"" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [_dialog dismissViewControllerAnimated:true completion:nil];
        }];
        [_dialog addAction:okAction];
    }
    return _dialog;
}

#pragma mark - Utils

- (NSString *)formatDate:(NSDate *)dateToFormat {
    return [NSDateFormatter localizedStringFromDate:dateToFormat dateStyle:NSDateFormatterShortStyle timeStyle:NSDateFormatterNoStyle];
}

- (Score *)scoreForIndexPath:(NSIndexPath *)indexPath {
    return self.scoresArray[indexPath.row];
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
