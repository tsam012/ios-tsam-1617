//
//  ViewController.h
//  TapChallenge
//
//  Created by Damiano Giusti on 13/01/17.
//  Copyright © 2017 Damiano Giusti. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ScoreTableViewController.h"

@interface GameViewController : UIViewController <ScoreTableViewDelegate>

@property (nonatomic, weak) IBOutlet UILabel *tapsCountLabel;

@property (nonatomic, weak) IBOutlet UILabel *timeLabel;

@property (nonatomic, weak) IBOutlet UITapGestureRecognizer *tapGestureRecognizer;

- (IBAction)tapGestureRecognizerDidRecognizeTap:(id)sender;

@end

