//
//  ViewController.m
//  TapChallenge
//
//  Created by Damiano Giusti on 13/01/17.
//  Copyright © 2017 Damiano Giusti. All rights reserved.
//

#import "GameViewController.h"
#import "PreferenceManager.h"
#import "ScoreTableViewController.h"

#define GameTimer 1
#define GameTime 30


@interface GameViewController () {
    int _tapsCount;
    int _timeCount;
    NSTimer *_gameTimer;
    
    UILabel *label;
}

@end

@implementation GameViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    [self initializeGame];
    [_tapGestureRecognizer addTarget:self action:@selector(onScreenPressed:)];
    
    // setto il titolo della navigation bar
    [self setTitle:@"Tap Challenge"];
    
    // creo un pulsante che andrò a mettere dentro la navigation bar
    UIBarButtonItem *scoreButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemBookmarks target:self action:@selector(scoreButtonPressed)];
    
    // imposto il pulsante come elemento alla destra della navigation bar
    self.navigationItem.rightBarButtonItem = scoreButtonItem;
    
    // creo una label e la inserisco
    /*
    label = [[UILabel alloc] initWithFrame:CGRectMake(10, 100, 300, 40)];
    [label setText:@"Label da codice"];
    [label setTextColor:[UIColor redColor]];
    [UIView animateWithDuration:5 animations:^{
        [label setCenter:CGPointMake(300, 500)];
        [label setAlpha:0];
    }];
    [self.view addSubview:label];
    */
}

- (void)viewDidAppear:(BOOL)animated {
    
    if ([PreferenceManager firstAppLaunch]) {
        [PreferenceManager markAsLaunched];
    } else {
        NSArray *results = [self getResults];
        if (results.count > 0)
            [self showResult:[((Score *) results.lastObject) score]];
    }
    
    [self resumeGame];
}

- (void)viewWillDisappear:(BOOL)animated {
    [self pauseGame];
}

- (IBAction)tapGestureRecognizerDidRecognizeTap:(id)sender {
    // NSLog(@"premuto con action");
}

- (void)onScreenPressed:(UITapGestureRecognizer *)recognizer {
    // creo il timer solo se non è gia in esecuzione
    if (_gameTimer == nil) {
        _gameTimer = [NSTimer scheduledTimerWithTimeInterval:GameTimer target:self selector:@selector(timerTick) userInfo:nil repeats:true];
        [self updateTimeLabel];
    }
    _tapsCount++;
    // NSLog(@"button pressed: %i", _tapsCount);
    [self updateCountLabel];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Game

- (void)initializeGame {
    _tapsCount = 0;
    _timeCount = GameTime;
    
    [self.tapsCountLabel setText:@"Tap To Play"];
    [self.timeLabel setText:@"Are you ready?"];
}

- (void)pauseGame {
    if (_gameTimer != nil) {
        [_gameTimer invalidate];
        _gameTimer = nil;
    }
}

- (void)resumeGame {
    if (_timeCount > 0 && _tapsCount > 0) {
        _gameTimer = [NSTimer scheduledTimerWithTimeInterval:GameTimer target:self selector:@selector(timerTick) userInfo:nil repeats:true];
    }
}

#pragma mark - UIActions

- (void)timerTick {
    // NSLog(@"%s", __PRETTY_FUNCTION__);
    
    _timeCount--;
    [self updateTimeLabel];
    
    if (_timeCount == 0) {
        [self gameDidFinish];
    }
}

- (void)scoreButtonPressed {
    NSLog(@"score pressed");
    // pusho all'interno dello stack del navigation controller un nuovo view controller
    /*UIViewController *viewController = [[UIViewController alloc] init];
    [self.navigationController pushViewController:viewController animated:true];*/
    
    ScoreTableViewController *tableViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ScoreTableViewController"];
    //[tableViewController setScoresArray:[self getResults]];
    [tableViewController setDelegate:self];
    
    [self.navigationController pushViewController:tableViewController animated:true];

}

#pragma mark - UI utils

- (void) createAlertController:(void (^)())handler {
    NSString *message = [NSString stringWithFormat:@"Hai fatto %i Taps!", _tapsCount];
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Game Over!" message:message preferredStyle:UIAlertControllerStyleAlert];
    

    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        handler();
    }];
    
    [alertController addAction:okAction];
    
    [self presentViewController:alertController animated:true completion:nil];
}

- (void)updateCountLabel {
    [self.tapsCountLabel setText:[NSString stringWithFormat:@"%i", _tapsCount]];
}

- (void)updateTimeLabel {
    [self.timeLabel setText:[NSString stringWithFormat:@"Ti rimangono %i secondi", _timeCount]];
}

- (void)gameDidFinish {
    [_gameTimer invalidate];
    _gameTimer = nil;
    
    [self storeResult];
    
    [self createAlertController:^{
        [self initializeGame];
    }];
}

- (void)showResult:(NSInteger) result {
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Wall of fame" message:[NSString stringWithFormat:@"Miglior risultato: %li", result]  preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okAlertAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
    
    [alertController addAction:okAlertAction];
    [self presentViewController:alertController animated:true completion:nil];

}

#pragma mark - Persistence

- (void)storeResult {
    
    Score *score = [[Score alloc] initWithScore:_tapsCount andDate:[NSDate date]];
    [PreferenceManager storeResult:score];
}

- (NSArray*)getResults {

    return [PreferenceManager getResults];
}

#pragma mark - ScoreTableViewDelegate

- (NSArray *)scoreTableViewFetchResult {
    return [self getResults];
}

- (void)scoreTableViewDidFetchResult {
    NSLog(@"%s", __PRETTY_FUNCTION__);
}

@end
