//
//  ScoreTableViewController.h
//  TapChallenge
//
//  Created by Damiano Giusti on 18/01/17.
//  Copyright © 2017 Damiano Giusti. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ScoreTableViewDelegate <NSObject>

/// Richiedo i dati alla classe chiamante
- (NSArray *)scoreTableViewFetchResult;

/// Informo il chiamante che ho terminato il fetch dei dati
- (void)scoreTableViewDidFetchResult;

@end

@interface ScoreTableViewController : UITableViewController

@property (nonatomic, strong) NSArray *scoresArray;

@property (nonatomic, unsafe_unretained) id <ScoreTableViewDelegate> delegate;

@end
