//
//  PreferenceManager.m
//  TapChallenge
//
//  Created by Damiano Giusti on 17/01/17.
//  Copyright © 2017 Damiano Giusti. All rights reserved.
//

#import "PreferenceManager.h"

#define kFirstAppLaunch @"IsFirstLaunch"
#define kResults @"kResults"
#define Defaults [NSUserDefaults standardUserDefaults]

@implementation PreferenceManager

+ (void)storeResult:(Score *)result {
    NSMutableArray *array = [[Defaults objectForKey:kResults] mutableCopy];
    if (array == nil) {
        // OLD way
        // array = [[NSMutableArray alloc] init].mutableCopy;
        
        // NEW way
        array = [[NSMutableArray alloc] init];
    }
    
    // OLD way
    // NSNumber *number = [NSNumber numberWithInteger:_tapsCount];
    // [array addObject:number];
    
    // NEW way
    [array addObject:[NSKeyedArchiver archivedDataWithRootObject:result]];
    
    NSLog(@"Array: %@", array);
    
    [Defaults setObject:array forKey:kResults];
    [Defaults synchronize];
}

+ (NSArray *)getResults {
    NSMutableArray *array = [[Defaults objectForKey:kResults] mutableCopy];
    
    if (array == nil)
        array = @[].mutableCopy;
    
    for (int i = 0; i < [array count]; ++i) {
        array[i] = [NSKeyedUnarchiver unarchiveObjectWithData:array[i]];
    }
    NSArray *resultsArray = [array sortedArrayUsingComparator:^NSComparisonResult(Score * _Nonnull obj1, Score *  _Nonnull obj2) {
        NSInteger value1 = obj1.score;
        NSInteger value2 = obj2.score;
        
        if (value1 == value2)
            return NSOrderedSame;
        if (value1 < value2)
            return NSOrderedAscending;
        return NSOrderedDescending;
    }];
    
    NSLog(@"letto l'array: %@", resultsArray);
    return resultsArray;
}

+ (Score *)decodeData:(id)object {
    return [NSKeyedUnarchiver unarchiveObjectWithData:object];
}

+ (BOOL)hasResult {
    return [self getResults] != nil;
}

+ (void)markAsLaunched {
    [Defaults setBool:NO forKey:kFirstAppLaunch];
    [Defaults synchronize];
}

+ (BOOL)firstAppLaunch {
    return [Defaults boolForKey:kFirstAppLaunch];
}

@end


