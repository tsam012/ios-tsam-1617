//
//  Utils.h
//  Stoccaggio
//
//  Created by Damiano Giusti on 08/02/17.
//  Copyright © 2017 Damiano Giusti. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#import "Corriere.h"

/** Peso ferro in grammi su cm3 */
FOUNDATION_EXPORT const NSInteger kPesoSpecificoFerro;

/** Peso plastica in grammi su cm3 */
FOUNDATION_EXPORT const NSInteger kPesoSpecificoPlastica;

/** Peso carta in grammi su cm3 */
FOUNDATION_EXPORT const NSInteger kPesoSpecificoCarta;

@interface Utils : NSObject

+ (int)randomNumberBetween:(int)from to:(int)to;

+ (UIImage *)imageForCorriere:(Corriere *)corriere;

@end
