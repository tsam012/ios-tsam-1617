//
//  Utils.m
//  Stoccaggio
//
//  Created by Damiano Giusti on 08/02/17.
//  Copyright © 2017 Damiano Giusti. All rights reserved.
//

#import "Utils.h"

NSInteger const kPesoSpecificoFerro = 8;

NSInteger const kPesoSpecificoPlastica = 2;

NSInteger const kPesoSpecificoCarta = 1;

@implementation Utils

+ (int)randomNumberBetween:(int)from to:(int)to {
    return (int)from + arc4random() % (to-from+1);
}

+ (UIImage *)imageForCorriere:(Corriere *)corriere {
    return [UIImage imageNamed:corriere.nome];
}

@end
