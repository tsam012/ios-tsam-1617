//
//  Pacco.h
//  Stoccaggio
//
//  Created by Damiano Giusti on 08/02/17.
//  Copyright © 2017 Damiano Giusti. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, Materiale) {
    MaterialeFerro = 0,
    MaterialePlastica = 1,
    MaterialeCarta = 2
};

@interface Pacco : NSObject

- (id)initWithCodice:(NSString *)codice
            mittente:(NSString *)mittente
        destinatario:(NSString *)destinatario
           lunghezza:(NSInteger)lunghezza
             altezza:(NSInteger)altezza
          profondita:(NSInteger)profondita
        andMateriale:(Materiale)materiale;

/** Codice del pacco */
@property (strong, nonatomic, readonly) NSString *codice;

/** Il mittente che ha spedito il pacco */
@property (strong, nonatomic) NSString *mittente;

/** Il destinatario della spedizione */
@property (strong, nonatomic) NSString *destinatario;

/** La lunghezza del pacco */
@property (nonatomic, readonly) NSInteger lunghezza;

/** L'altezza del pacco */
@property (nonatomic, readonly) NSInteger altezza;

/** La profondità del pacco */
@property (nonatomic, readonly) NSInteger profondita;

/** Volume del pacco */
@property (nonatomic, readonly) NSInteger volume;

/** Peso del pacco */
@property (nonatomic, readonly) NSInteger peso;

/** Tipologia materiale contenuta nel pacco */
@property (nonatomic, readonly) Materiale materiale;

@end
