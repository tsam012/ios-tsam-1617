//
//  Pacco.m
//  Stoccaggio
//
//  Created by Damiano Giusti on 08/02/17.
//  Copyright © 2017 Damiano Giusti. All rights reserved.
//

#import "Pacco.h"

#import "Utils.h"

@implementation Pacco

- (id)initWithCodice:(NSString *)codice
            mittente:(NSString *)mittente
        destinatario:(NSString *)destinatario
           lunghezza:(NSInteger)lunghezza
             altezza:(NSInteger)altezza
          profondita:(NSInteger)profondita
        andMateriale:(Materiale)materiale {
    
    // chiamo il costruttore dell'oggetto base
    self = [super init];
    
    // controllo se il costruttore base ha allocato l'oggetto
    if (self) {
        _codice = codice;
        _mittente = mittente;
        _destinatario = destinatario;
        _lunghezza = lunghezza;
        _altezza = altezza;
        _profondita = profondita;
        _materiale = materiale;
    }
    
    // ritorno "me stesso" allocato o meno
    return self;
}

#pragma mark - Getters

- (NSInteger)volume {
    return _lunghezza * _altezza * _profondita;
}

- (NSInteger)peso {
    switch (_materiale) {
        case MaterialeCarta:
            return kPesoSpecificoCarta * self.volume;
        case MaterialeFerro:
            return kPesoSpecificoFerro * self.volume;
        case MaterialePlastica:
            return kPesoSpecificoPlastica * self.volume;
        default:
            [NSException raise:@"Calcolo del peso per un tipo di materiale non noto." format:@""];
    }
}

#pragma mark - Overrides

- (NSString *)description {
    NSString *format = @"%@\nCodice: %@\nMittente: %@\nDestinatario: %@\nLunghezza: %li cm\nAltezza: %li cm\nProfondità: %li cm\nMateriale: %li\nVolume: %li cm3\nPeso: %g kg";
    return [NSString stringWithFormat:format, [super description], self.codice, self.mittente, self.destinatario, self.lunghezza, self.altezza, self.profondita, self.materiale, self.volume, self.peso / 1000.0];
}

@end
