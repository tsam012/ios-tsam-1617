//
//  Corriere.m
//  Stoccaggio
//
//  Created by Damiano Giusti on 08/02/17.
//  Copyright © 2017 Damiano Giusti. All rights reserved.
//

#import "Corriere.h"

@interface Corriere()

@property (nonatomic) NSInteger spazioRimanente;

@property (nonatomic) NSInteger pesoTot;

@end

@implementation Corriere

- (instancetype)initWithNome:(NSString *)nome volumeMax:(NSInteger)volumeMax andPesoMax:(NSInteger)pesoMax {
    self = [super init];
    
    if (self) {
        _nome = nome;
        _volumeMax = volumeMax;
        _pesoMax = pesoMax;
        
        _spazioRimanente = volumeMax;
        _pesoTot = 0;
    }
    
    return self;
}

- (NSMutableArray *)pacchi {
    if (!_pacchi)
        _pacchi = [[NSMutableArray alloc] init];
    return _pacchi;
}

- (BOOL)aggiungiPacco:(Pacco *)pacco {
    NSInteger volumePacco = pacco.volume;
    NSInteger pesoPacco = pacco.peso;
    
    if (self.spazioRimanente < volumePacco || (self.pesoTot + pesoPacco) > self.pesoMax)
        return NO;
    
    [self.pacchi addObject:pacco];
    
    self.spazioRimanente -= volumePacco;
    self.pesoTot += pesoPacco;
    return YES;
}

- (BOOL)haSpazioPerIlPacco:(Pacco *)pacco {
    NSInteger volumePacco = pacco.volume;
    NSInteger pesoPacco = pacco.peso;
    
    return self.spazioRimanente >= volumePacco && (self.pesoTot + pesoPacco) <= self.pesoMax;
}

@end
