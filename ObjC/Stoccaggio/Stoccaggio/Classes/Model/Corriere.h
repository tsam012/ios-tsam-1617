//
//  Corriere.h
//  Stoccaggio
//
//  Created by Damiano Giusti on 08/02/17.
//  Copyright © 2017 Damiano Giusti. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "Pacco.h"

@interface Corriere : NSObject

@property (strong, nonatomic, readonly) NSString *nome;

@property (strong, nonatomic) NSMutableArray *pacchi;

@property (nonatomic, readonly) NSInteger volumeMax;

@property (nonatomic, readonly) NSInteger pesoMax;

- (instancetype)initWithNome:(NSString *)nome volumeMax:(NSInteger)volumeMax andPesoMax:(NSInteger)pesoMax;

- (BOOL)aggiungiPacco:(Pacco *)pacco;

- (BOOL)haSpazioPerIlPacco:(Pacco *)pacco;

@end
