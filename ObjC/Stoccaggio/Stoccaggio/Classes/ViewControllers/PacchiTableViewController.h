//
//  PacchiTableViewController.h
//  Stoccaggio
//
//  Created by Damiano Giusti on 13/02/17.
//  Copyright © 2017 Damiano Giusti. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "Corriere.h"

@protocol PacchiTableViewDataSourceDelegate <NSObject>



@end

@interface PacchiTableViewController : UITableViewController <UITableViewDelegate>

@property (strong, nonatomic) Corriere *corriere;

@end
