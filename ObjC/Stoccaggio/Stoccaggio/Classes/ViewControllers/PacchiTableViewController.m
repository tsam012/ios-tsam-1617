//
//  PacchiTableViewController.m
//  Stoccaggio
//
//  Created by Damiano Giusti on 13/02/17.
//  Copyright © 2017 Damiano Giusti. All rights reserved.
//

#import "PacchiTableViewController.h"

@interface PacchiTableViewController ()

@end

@implementation PacchiTableViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    
    // [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"PaccoTableViewCell"];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    [self setTitle:[NSString stringWithFormat:@"Pacchi di %@", self.corriere.nome]];
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.corriere.pacchi count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"PaccoTableViewCell" forIndexPath:indexPath];
    
    Pacco *pacco = [self paccoFromIndexPath:indexPath];
    
    NSString *text = [NSString stringWithFormat:@"%@ - %@", pacco.codice, pacco.destinatario];
    NSString *description = [NSString stringWithFormat:@"Peso: %.3f kg ; Volume: %li cm3", (float) pacco.peso / 1000, pacco.volume];
    
    [cell.textLabel setText:text];
    [cell.detailTextLabel setText:description];
    [cell.imageView setImage:[UIImage imageNamed:@"Pacco"]];
    
    return cell;
}


- (Pacco *)paccoFromIndexPath:(NSIndexPath *)indexPath {
    return self.corriere.pacchi[indexPath.row];
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
