//
//  ViewController.m
//  Stoccaggio
//
//  Created by Damiano Giusti on 08/02/17.
//  Copyright © 2017 Damiano Giusti. All rights reserved.
//

#import "CorrieriTableViewController.h"

#import "PacchiTableViewController.h"
#import "Corriere.h"
#import "Pacco.h"
#import "Utils.h"

#define PacchiTot 10000

@interface CorrieriTableViewController () {
    NSMutableArray *corrieri;
}

@end

@implementation CorrieriTableViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    corrieri = [[NSMutableArray alloc] init];
    
    Corriere *bartolini = nil;
    
    Pacco *pacco = [self paccoRandom];
    
    for (int i = 0; i < PacchiTot; ++i) {
        
        if (!bartolini)
            bartolini = [self corriereRandom];
        
        if ([bartolini haSpazioPerIlPacco:pacco]) {
            [bartolini aggiungiPacco:pacco];
            pacco = [self paccoRandom];
        } else {
            [corrieri addObject:bartolini];
            bartolini = nil;
        }
    }
    
    if (bartolini)
       [corrieri addObject:bartolini];
    

    NSLog(@"Per %i pacchi servono %li corrieri", PacchiTot, [corrieri count]);
    
    
    [self setTitle:@"Corrieri"];
}

#pragma mark - TableView


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [corrieri count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TableViewCell" forIndexPath:indexPath];
    
    Corriere *corriere = [self corriereFromIndexPath:indexPath];
    
    [cell.textLabel setText:corriere.nome];
    [cell.detailTextLabel setText:[NSString stringWithFormat:@"%li pacchi", [corriere.pacchi count]]];
    [cell.imageView setImage:[Utils imageForCorriere:corriere]];
    
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    /*
    Corriere *corriere = [self corriereFromIndexPath:indexPath];
    
    PacchiTableViewController *nextViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"PacchiTableViewController"];
    
    nextViewController.corriere = corriere;
    
    [self.navigationController pushViewController:nextViewController animated:YES];
    */
}


- (Corriere *)corriereFromIndexPath:(NSIndexPath *)indexPath {
    return corrieri[indexPath.row];
}

#pragma mark - Navigation


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    PacchiTableViewController *nextViewController = segue.destinationViewController;
    
    nextViewController.corriere = [self corriereFromIndexPath:[self.tableView indexPathForSelectedRow]];
}

#pragma mark - Random

- (Corriere *)corriereRandom {
    return [self corriereRandomConNome: [self nomeCorriereRandom]];
}

- (NSString *)nomeCorriereRandom {
    NSInteger rand = [Utils randomNumberBetween:0 to:4];
    
    switch (rand) {
        case 0:
            return @"Bartolini";
        case 1:
            return @"DHL";
        case 2:
            return @"TNT";
        case 3:
            return @"FedEx";
        case 4:
            return @"GLS";
            
    }
    return @"Poste Italiane";
}

- (Corriere *)corriereRandomConNome:(NSString *)nome {
    return [[Corriere alloc] initWithNome:nome
                                volumeMax:[Utils randomNumberBetween:18000 to:23000]
                               andPesoMax:[Utils randomNumberBetween:700000 to:850000]];
}


- (Pacco *)paccoRandom {
    
    return [[Pacco alloc] initWithCodice:[self targaRandom]
                                mittente:@"Damiano"
                            destinatario:[self nomeRandom]
                               lunghezza:[Utils randomNumberBetween:5 to:10]
                                 altezza:[Utils randomNumberBetween:5 to:10]
                              profondita:[Utils randomNumberBetween:5 to:10]
                            andMateriale:[Utils randomNumberBetween:0 to:2]];
}



- (NSString *)nomeRandom {
    NSInteger rand = [Utils randomNumberBetween:0 to:4];
    
    switch (rand) {
        case 0:
            return @"Giorgio";
        case 1:
            return @"Andrea";
        case 2:
            return @"Giovanni";
        case 3:
            return @"Filippo";
        case 4:
            return @"Mastro lindo";
            
    }
    return @"Poste Italiane";
}

- (NSString *)targaRandom {
    return [NSString stringWithFormat:@"%c%c%03i%c%c",
            [Utils randomNumberBetween:0 to:25] + 'A',
            [Utils randomNumberBetween:0 to:25] + 'A',
            [Utils randomNumberBetween:0 to:999],
            [Utils randomNumberBetween:0 to:25] + 'A',
            [Utils randomNumberBetween:0 to:25] + 'A'];
}

@end
