//
//  main.m
//  SingleViewApplication
//
//  Created by Damiano Giusti on 11/01/17.
//  Copyright © 2017 Damiano Giusti. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
