//
//  ViewController.h
//  SingleViewApplication
//
//  Created by Damiano Giusti on 11/01/17.
//  Copyright © 2017 Damiano Giusti. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

// IBOutlet -> Interface Builder Outlet
@property (nonatomic, weak) IBOutlet UILabel *helloWorldLabel;

@property (nonatomic, weak) IBOutlet UITextField *userNameTextField;

-(IBAction)userNameTextFieldDidEndOnExit:(id)sender;

-(IBAction)userNameTextFieldEditingDidEnd:(id)sender;

-(IBAction)buttonPressed:(id)sender;

@end

