//
//  ViewController.m
//  SingleViewApplication
//
//  Created by Damiano Giusti on 11/01/17.
//  Copyright © 2017 Damiano Giusti. All rights reserved.
//

#import "ViewController.h"

@interface ViewController () {
    int _tapCount;
}

@end

@implementation ViewController

int _contaPressioni;

-(void)awakeFromNib {
    [super awakeFromNib];
    NSLog(@"%s", "awakeFromNib");
    // le view IBOutlet non sono inizializzate
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    /*
     Tutti i componenti con il prefisso NS (Next Step) fanno parte del framework Foundation.
     Tutti i componenti con il prefisso UI, fanno parte del framework UIKit, che contiene i componenti della UI
     */
    
    // le view IBOutlet sono inizializzate
    
    _tapCount = 0;
    
    [self.helloWorldLabel setTextColor:[UIColor blackColor]];
    [self.helloWorldLabel setText:[NSString stringWithFormat:@"%d", _tapCount]];
    
    NSLog(@"%s", "viewDidLoad");
    
    [self updateText:@"viewDidLoad" toUILabel:self.helloWorldLabel];
    
    [self proveArray];
    
    [self.userNameTextField setText:@"Mario"];
    
}

- (void)viewWillAppear:(BOOL)animated {
    NSLog(@"%s", "viewWillAppear");
}

- (void)viewDidAppear:(BOOL)animated {
    NSLog(@"%s", "viewDidAppear");
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - IBActions

- (IBAction)userNameTextFieldDidEndOnExit:(id)sender {
    NSLog(@"userNameTextFieldDidEndOnExit");
}

- (IBAction)userNameTextFieldEditingDidEnd:(UITextField *)sender {
    NSLog(@"userNameTextFieldEditingDidEnd");
    NSLog(@"%@", sender.text);
    [self.helloWorldLabel setText:[NSString stringWithFormat:@"%@", sender.text]];
}

- (IBAction)buttonPressed:(id)sender {
    _tapCount++;
    
    NSLog(@"%d", _tapCount);
    
    [self.helloWorldLabel setText:[NSString stringWithFormat:@"%d", _tapCount]];
}

#pragma mark - utils

- (void)updateText:(NSString *) newText toUILabel:(UILabel *) label {
    [label setText: newText];
}


- (void)printTestLogs {
    NSString *string = @"Hello world";
    const int i = 2;
    
    NSLog(@"%@ %s%d", string, "#", i);
}

- (void)proveArray {
    const int count = 5;
    
    for (int i = 0; i < count; i++) {
        NSString *string = [NSString stringWithFormat:@"#%i", i];
        [self updateText:string toUILabel:self.helloWorldLabel];
    }
    
    NSArray *array = @[@"prima stringa", @"seconda stringa", @1];
    // di un oggetto è possibile stampare la "description", formattandolo come oggetto Foundation
    NSLog(@"%@", array);
    
    NSArray *secondFixedArray = [[NSArray alloc] initWithObjects:@"prima stringa", @"seconda stringa", @1, nil];
    
    NSMutableArray *mutableArray = @[@"primo elemento"].mutableCopy;
    [mutableArray addObject:@"secondo elemento"];
    NSLog(@"%@", mutableArray);
}


@end
