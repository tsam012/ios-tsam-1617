//
//  AppDelegate.m
//  MacAppDemo
//
//  Created by Damiano Giusti on 05/02/17.
//  Copyright © 2017 Damiano Giusti. All rights reserved.
//

#import "AppDelegate.h"

@interface AppDelegate ()

@end

@implementation AppDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {
    // Insert code here to initialize your application
}


- (void)applicationWillTerminate:(NSNotification *)aNotification {
    // Insert code here to tear down your application
}


@end
