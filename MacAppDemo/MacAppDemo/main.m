//
//  main.m
//  MacAppDemo
//
//  Created by Damiano Giusti on 05/02/17.
//  Copyright © 2017 Damiano Giusti. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[]) {
    return NSApplicationMain(argc, argv);
}
