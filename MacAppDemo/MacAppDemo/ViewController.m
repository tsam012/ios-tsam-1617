//
//  ViewController.m
//  MacAppDemo
//
//  Created by Damiano Giusti on 05/02/17.
//  Copyright © 2017 Damiano Giusti. All rights reserved.
//

#import "ViewController.h"

@interface ViewController()

@property (weak, nonatomic) IBOutlet NSTextField *textField;


@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    // Do any additional setup after loading the view.
}

- (void)viewWillAppear {
    [super viewWillAppear];
}


- (void)setRepresentedObject:(id)representedObject {
    [super setRepresentedObject:representedObject];

    // Update the view, if already loaded.
}

- (IBAction)buttonPressed:(NSButton *)sender {
    NSLog(@"%@", self.textField.stringValue);
}

@end
