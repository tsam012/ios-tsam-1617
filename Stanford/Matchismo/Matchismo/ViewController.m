//
//  ViewController.m
//  Matchismo
//
//  Created by Damiano Giusti on 29/01/17.
//  Copyright © 2017 Damiano Giusti. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@property (weak, nonatomic) IBOutlet UILabel *cardNameLabel;

@end

@implementation ViewController

- (IBAction)cardButtonPressed:(UIButton *)sender {

    if ([[self.cardNameLabel text] length] == 0) {
        [self.cardNameLabel setText:@"BITCH"];
        [sender setBackgroundImage:[UIImage imageNamed:@"cardfront"] forState:UIControlStateNormal];
    } else {
        [sender setBackgroundImage:[UIImage imageNamed:@"cardback"] forState:UIControlStateNormal];
        [self.cardNameLabel setText:@""];
    }
}

@end
