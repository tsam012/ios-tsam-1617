//
//  PlayingDeck.m
//  Matchismo
//
//  Created by Damiano Giusti on 29/01/17.
//  Copyright © 2017 Damiano Giusti. All rights reserved.
//

#import "PlayingDeck.h"
#import "PlayingCard.h"

@implementation PlayingDeck

- (instancetype)init {
    self = [super init];
    
    if (self) {
        for (NSString *suit in [PlayingCard validSuits]) {
            for (NSUInteger i = 1; i <= [PlayingCard maxRank]; i++) {
                PlayingCard *card = [[PlayingCard alloc] init];
                card.suit = suit;
                card.rank = i;
                [self addCard:card];
            }
        }
    }
    
    return self;
}

@end
