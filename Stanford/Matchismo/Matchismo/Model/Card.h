//
//  Card.h
//  Matchismo
//
//  Created by Damiano Giusti on 29/01/17.
//  Copyright © 2017 Damiano Giusti. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Card : NSObject

@property (strong, nonatomic) NSString *contents;

// per i tipi primitivi non serve specificare la tipologia di reference
// si può personalizzare il nome dei get e set con un attributo

@property (nonatomic, getter=isChosen) BOOL chosen;

@property (nonatomic, getter=isMatched) BOOL matched;

- (int)match:(NSArray *)otherCards;

@end
