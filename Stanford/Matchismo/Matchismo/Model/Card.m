//
//  Card.m
//  Matchismo
//
//  Created by Damiano Giusti on 29/01/17.
//  Copyright © 2017 Damiano Giusti. All rights reserved.
//

#import "Card.h"

@implementation Card

- (int)match:(NSArray *)otherCards {
    int matches = 0;
    
    for (Card *card in otherCards) {
        if ([self.contents isEqualToString:card.contents])
            matches = 1;
    }
    
    return matches;
}

@end
